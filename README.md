# Mini Project 10: Rust Serverless LLM Endpoint
## Project Description
In this project, we are asked to write a function to ultilize Hugging Face Rust transformer, dockerize it and deploy container to AWS Lambda. This project requires us to use both AWS Lambda and AWS ECR.

## Model Selection
For this project, I select a model from [Rustformer HuggingFace](https://huggingface.co/rustformers). Typically I choose the model pythia-1.4b-q5_1-ggjt.bin since it's only 1.06G and small enough to hold on Docker and AWS. You could store this model in a new folder or `src` folder. Since the model is too large, I didn't attach it in the repo. With this model, my function allows user to type in their own words and the ouput will generates randomly to complete the sentence user passed in.

## Detailed steps
1. Initialize your project by `cargo lambda new <YOUR PROJECT NAME>`.
2. Start you implementation of the `main.rs`. My implementation follows the [examples](https://github.com/rustformers/llm/blob/main/crates/llm/examples/inference.rs) located in the rustformers repo. Make sure you download the corresponding `.bin` file from HuggingFace and put it in your own directory.
3. Create a corresponding `Dockerfile` and `Makefile`. You can simply copy mine.
4. Now, you can use `cargo lambda watch` to test if there's any error in your function. Then you can use either:
    ```bash
    cargo lambda invoke --data-ascii '{"requestContext":{"http":{"method":"GET","path":"/example"}},"queryStringParameters":{"query":"Example query text"},"headers":{},"body":"","isBase64Encoded":false}'
    ```
    or
    ```bash
    cargo lambda invoke --data-file event.json
    ```
    with the example `event.json` I created in the repo to test your output. Replace "Example query text" with your own words.
5. Go to `AWS IAM`. Create a new user with the following policies: `iamfullaccess`, `lambdafullaccess` and `AmazonEC2ContainerRegistryFullAccess`. Then go to `Secruity Credentials` to create new access key. 
6. Create a `.env` file and add your two access keys and access region to the file with the following structure:
    ```plaintext
    AWS_ACCESS_KEY_ID=
    AWS_SECRET_ACCESS_KEY=
    AWS_REGION=
    ```
    Then create a `.gitignore` file:
    ```plaintext
    /target
    .env
    ```
    Then use either `Export` or `set -a` and `source .env` to allow your terminal to detect your AWS user
7. Go to `AWS ECR` to create a new repo. Then use:
    ```bash
    aws ecr get-login-password --region your-region | docker login --username AWS --password-stdin your-aws-account-id.dkr.ecr.your-region.amazonaws.com
    ```
    to login into your ECR.
8. Then use the following command to build and push your function to ECR:
    ```bash
    docker buildx build --progress=plain --platform linux/arm64 -t mini-10 .
    docker tag mini-10:latest 590183705245.dkr.ecr.us-east-1.amazonaws.com/mini-proj-10:latest
    docker push 590183705245.dkr.ecr.us-east-1.amazonaws.com/mini-proj-10:latest
    ```
9. After the pushing, go to `AWS Lambda` to create a new function using `Container Image`. Make sure that you select `arm64`.
10. After deploying, go to `General Configuration` to change the memory and timeout based on your model. Then go to `Function url` to new function URL with `CORS enabled`.
11. Now you could use the given event.json file to test the function.

## cURL Endpoints
You could use the following command to test your url:
```
curl -X POST "https://33oi3n7ywbh3mibm573rvrmwvq0oostv.lambda-url.us-east-1.on.aws/" \
     -H "Content-Type: application/json" \
     -d '{"requestContext":{"http":{"method":"GET","path":"/example"}},"queryStringParameters":{"query":"Example query text"},"headers":{},"body":"","isBase64Encoded":false}'
```
## Screenshots
- Function output
![](https://gitlab.com/HathawayLiu/mini_proj_10_hathaway_liu/-/wikis/uploads/18861b99de4271e0fa3a34e71eb9cc87/Screenshot_2024-04-14_at_12.36.28_AM.png)
- Lambda functions and logs
![](https://gitlab.com/HathawayLiu/mini_proj_10_hathaway_liu/-/wikis/uploads/cda4ce4afe6eb0c5383474317e4d92ff/Screenshot_2024-04-14_at_5.37.01_PM.png)
![](https://gitlab.com/HathawayLiu/mini_proj_10_hathaway_liu/-/wikis/uploads/9a6a1bc1e42f16edd1673d06a7c656f4/Screenshot_2024-04-14_at_5.38.02_PM.png)